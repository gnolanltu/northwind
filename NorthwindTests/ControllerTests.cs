using System;
using Xunit;

namespace NorthwindTests
{
    public class ControllerTests
    {
        [Fact]
        public void TestWeatherForecaseIsNotEmpty()
        {
            //arrange
            var controller = new ExploreAngular.Controllers.SampleDataController();

            //act
            var result = controller.WeatherForecasts();

            //assert
            Assert.NotEmpty(result);

        }
    }
}
